import 'package:get/get.dart';

class Dimensions{
  static double screenHeight = Get.context!.height;
  static double screenWidth = Get.context!.width;

  static double pageViewParentContainer = screenHeight/2.13; //683/120
  static double pageViewContainer = screenHeight/2.84; //683/240
  static double pageTextViewContainer = screenHeight/5.69; //683/120

  static double height15 = screenHeight/45.53; //683/15
  static double width10 = screenWidth/41.1; //411/10
  static double height10 = screenHeight/68.3; //683/15
  static double height50 = screenHeight/13.66; //683/50
  static double height20 = screenHeight/34.15; //683/20
  static double height30 = screenHeight/22.76; //683/30
  static double width30 = screenWidth/13.7; //411/30
  static double height12 = screenHeight/56.91; //683/12
  static double height1dot2 = screenHeight/569.16; //683/12
  static double width5 = screenWidth/82.2; //411/5
  static double width15 = screenWidth/27.4; //411/15
  static double height25 = screenHeight/27.32; //683/25
  static double height45 = screenHeight/15.17; //683/45
  static double width45 = screenWidth/9.13; //411/45
  static double radius15 = screenHeight/45.53; //683/15
  static double radius30 = screenHeight/22.76; //683/30
  static double radius5 = screenHeight/136.6; //683/5
  static double iconsize24 = screenHeight/26.58; //683/24

  static double listViewImageSize =  screenWidth/3.425; //411/120
  static double listViewTextContainerSize = screenWidth/4.11; //411/100






}