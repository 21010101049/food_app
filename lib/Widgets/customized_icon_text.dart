import 'package:flutter/material.dart';
import 'package:food_delivery_app/Utils/dimensions.dart';
import 'package:food_delivery_app/Widgets/small_text.dart';

class CustomizedIconText extends StatelessWidget{
  IconData icon;
  Color iconColor;
  String iconData;
  Color dataColor;

   CustomizedIconText({super.key,required this.icon,required this.iconColor, required this.iconData,required this.dataColor});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(icon,color: iconColor,size:Dimensions.screenWidth<=300?20:Dimensions.iconsize24),
        // SizedBox(width: 5,),
        SmallText(text: iconData,color: dataColor,),
      ],
    );
  }

}