import 'package:flutter/material.dart';
import 'package:food_delivery_app/Utils/dimensions.dart';

class BigText extends StatelessWidget {
  Color color;
  String text;
  TextOverflow overflow;
  double size;

  BigText(
      {super.key,
      required this.text,
      this.size = 0,
      this.overflow = TextOverflow.ellipsis,
      this.color = const Color(0xFF46D74A)});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: 1, //overflow when it increases from 1 line so given maxlines
      overflow: overflow,
      style: TextStyle(
        color: color,
        fontSize: size==0?Dimensions.height20:size,
        fontWeight: FontWeight.w400
      ),
    );
  }
}
