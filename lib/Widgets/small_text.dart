import 'package:flutter/material.dart';
import 'package:food_delivery_app/Utils/dimensions.dart';

class SmallText extends StatelessWidget {
  Color color;
  String text;
  double size;
  double height;

  SmallText(
      {super.key,
        required this.text,
        this.size = 0,
        this.height=0,
        this.color = const Color(0xFFADABAB)});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          color: color,
          fontSize: size==0?Dimensions.height12:size,
          height: height==0?Dimensions.height1dot2:height
      ),
    );
  }
}
