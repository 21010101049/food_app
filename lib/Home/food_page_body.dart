import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:food_delivery_app/Utils/colors.dart';
import 'package:food_delivery_app/Utils/dimensions.dart';
import 'package:food_delivery_app/Widgets/big_text.dart';
import 'package:food_delivery_app/Widgets/customized_icon_text.dart';
import 'package:food_delivery_app/Widgets/small_text.dart';

class FoodPageBody extends StatefulWidget {
  @override
  State<FoodPageBody> createState() => _FoodPageBodyState();
}

class _FoodPageBodyState extends State<FoodPageBody> {
  PageController pageController = PageController(viewportFraction: 0.85);
  var _currentPageValue = 0.0;
  double _scaleFactor = 0.8;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pageController.addListener(() {
      setState(() {
        _currentPageValue = pageController.page!;
        // print("current value is:${_currentPageValue.toString()}");
      });
    });
  }

  //when we leave that page dispose will tell memory that we have leaved and will not save memory

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("current screen height:${Dimensions.screenHeight}");
    print("current screen width:${Dimensions.screenWidth}");

    return Column(
      children: [
        //slider section
        Container(
          margin: EdgeInsets.all(10),
          // color: Colors.pink,
          height: Dimensions.pageViewParentContainer,
          child: PageView.builder(
            controller: pageController,
            itemCount: 5,
            itemBuilder: (context, index) {
              return _buildPageItem(index);
            },
          ),
        ),
        //dots
        DotsIndicator(
          dotsCount: 5,
          position: _currentPageValue,
          decorator: DotsDecorator(
            size: const Size.square(9.0),
            activeSize: const Size(18.0, 9.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Dimensions.radius5)),
          ),
        ),
        //Popular text
        SizedBox(
          height: Dimensions.height20,
        ),
        Container(
          // color: Colors.black,
          margin: EdgeInsets.only(left: Dimensions.width30),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              BigText(
                text: "Popular",
                color: Colors.black,
              ),
              SizedBox(
                width: Dimensions.width5,
              ),
              Container(
                margin: EdgeInsets.only(bottom: 3),
                child: SmallText(
                  text: ".",
                ),
              ),
              SizedBox(
                width: Dimensions.width5,
              ),
              Container(
                margin: EdgeInsets.only(bottom: 2),
                child: SmallText(text: "Food Pairing"),
              ),
            ],
          ),
        ),
        //List of food
        ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: 10,
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.only(
                  left: Dimensions.width15,
                  right: Dimensions.width15,
                  bottom: Dimensions.height10),
              child: Row(
                children: [
                  //image section
                  Container(
                    height: Dimensions.listViewImageSize,
                    width: Dimensions.listViewImageSize,
                    decoration: BoxDecoration(
                      border: Border.all(width: 1),
                      borderRadius:
                          BorderRadius.circular(Dimensions.radius30),
                      color: Colors.white,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/images/food0.jpeg"),
                      ),
                    ),
                  ),
                  //text section
                  Expanded(
                    child: Container(
                      height: Dimensions.listViewTextContainerSize,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(Dimensions.radius30),
                            bottomRight: Radius.circular(Dimensions.radius30),
                          ),
                          color: Colors.white),
                      child: Padding(
                        padding: EdgeInsets.only(left: Dimensions.width10,right: Dimensions.width10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            BigText(text: "Undhiyu sabji meal In India",color: Colors.black,),
                            // SizedBox(height: Dimensions.height10,),
                            SmallText(text: "With Gujarati Characteristics"),
                            // SizedBox(height: Dimensions.height10,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CustomizedIconText(
                                    icon: Icons.circle,
                                    iconColor: Colors.orangeAccent.shade200,
                                    iconData: "normal",
                                    dataColor: AppColors.textColor),
                                CustomizedIconText(
                                    icon: Icons.location_on,
                                    iconColor: Colors.greenAccent.shade200,
                                    iconData: "5 km",
                                    dataColor: AppColors.textColor),
                                CustomizedIconText(
                                    icon: Icons.access_time_rounded,
                                    iconColor: Colors.orange,
                                    iconData: "45 min",
                                    dataColor: AppColors.textColor),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ],
    );
  }

  Widget _buildPageItem(int index) {
    Matrix4 matrix = Matrix4.identity();
    if (index == _currentPageValue.floor()) {
      var currScale = 1 - (_currentPageValue - index) * (1 - _scaleFactor);
      var currTrans = Dimensions.screenHeight * (1 - currScale) / 2;
      matrix = Matrix4.diagonal3Values(1, currScale, 1)
        ..setTranslationRaw(0, currTrans, 0);
      // print("currScale:${currScale}");
      // print("matrix of ${index}:${Matrix4.diagonal3Values(1, currScale, 1)}");
    } else if (index == _currentPageValue.floor() + 1) {
      var currScale =
          _scaleFactor + (_currentPageValue - index + 1) * (1 - _scaleFactor);
      var currTrans = Dimensions.screenHeight * (1 - currScale) / 2;
      matrix = Matrix4.diagonal3Values(1, currScale, 1)
        ..setTranslationRaw(0, currTrans, 0);
    } else if (index == _currentPageValue.floor() - 1) {
      var currScale = 1 - (_currentPageValue - index) * (1 - _scaleFactor);
      var currTrans = Dimensions.screenHeight * (1 - currScale) / 2;
      matrix = Matrix4.diagonal3Values(1, currScale, 1)
        ..setTranslationRaw(0, currTrans, 0);
    } else {
      var currScale = 0.8;
      var currTrans = Dimensions.screenHeight * (1 - currScale) / 2;
      matrix = Matrix4.diagonal3Values(1, currScale, 1)
        ..setTranslationRaw(0, currTrans, 0);
    }

    return Transform(
      transform: matrix,
      child: Stack(
        // fit: StackFit.expand,
        children: [
          Container(
            height: Dimensions.pageViewContainer,
            margin: EdgeInsets.fromLTRB(Dimensions.width10, Dimensions.height10,
                Dimensions.width10, Dimensions.height50),
            decoration: BoxDecoration(
              border: Border.all(width: 1),
              borderRadius: BorderRadius.circular(Dimensions.radius30),
              color: index.isEven ? Colors.black : Colors.blue,
              image: DecorationImage(
                  image: AssetImage("assets/images/food0.jpeg"),
                  fit: BoxFit.cover),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: Dimensions.pageTextViewContainer,
              margin: EdgeInsets.fromLTRB(Dimensions.width30, 0,
                  Dimensions.width30, Dimensions.height10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(Dimensions.radius30),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Color(0xFFe8e8e8),
                      blurRadius: 5.0,
                      offset: Offset(0, 5)),
                  BoxShadow(color: Colors.white, offset: Offset(-5, 0)),
                  BoxShadow(color: Colors.white, offset: Offset(5, 0))
                ],
              ),
              child: Padding(
                padding: EdgeInsets.fromLTRB(
                    Dimensions.width10,
                    Dimensions.height10,
                    Dimensions.width10,
                    Dimensions.height10),
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BigText(
                      text: "Chinese Noodles",
                      color: Colors.black,
                    ),
                    SizedBox(
                      height: Dimensions.height10,
                    ),
                    Row(
                      children: [
                        Wrap(
                          children: List.generate(
                            5,
                            (index) => Icon(
                              Icons.star,
                              size: Dimensions.height15,
                              color: AppColors.mainColor,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: Dimensions.width10,
                        ),
                        SmallText(text: "5"),
                        SizedBox(
                          width: Dimensions.width5,
                        ),
                        SmallText(text: "1500"),
                        SizedBox(
                          width: Dimensions.width5,
                        ),
                        SmallText(text: "comments")
                      ],
                    ),
                    SizedBox(
                      height: Dimensions.height20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomizedIconText(
                            icon: Icons.circle,
                            iconColor: Colors.orangeAccent.shade200,
                            iconData: "normal",
                            dataColor: AppColors.textColor),
                        CustomizedIconText(
                            icon: Icons.location_on,
                            iconColor: Colors.greenAccent.shade200,
                            iconData: "5 km",
                            dataColor: AppColors.textColor),
                        CustomizedIconText(
                            icon: Icons.access_time_rounded,
                            iconColor: Colors.orange,
                            iconData: "45 min",
                            dataColor: AppColors.textColor),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
