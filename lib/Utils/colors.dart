import 'dart:ui';

class AppColors{
  static final Color mainColor = const Color(0xFF46D74A);
  static final Color textColor = const Color(0xFFADABAB);
}