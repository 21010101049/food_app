import 'package:flutter/material.dart';
import 'package:food_delivery_app/Home/food_page_body.dart';
import 'package:food_delivery_app/Utils/dimensions.dart';
import 'package:food_delivery_app/Widgets/big_text.dart';
import 'package:food_delivery_app/Widgets/small_text.dart';
import 'package:food_delivery_app/Utils/colors.dart';

class MainFoodPage extends StatefulWidget{
  @override
  State<MainFoodPage> createState() => _MainFoodPageState();
}

class _MainFoodPageState extends State<MainFoodPage> {
  @override
  Widget build(BuildContext context) {
    print("iconsize:${Dimensions.iconsize24}");
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              child: Container(
                margin: EdgeInsets.only(left: Dimensions.width15,right: Dimensions.width15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                      Column(
                        children: [
                          BigText(text: "America",size: Dimensions.height25,),
                          Row(
                            children: [
                              SmallText(text: "Gathiya",size: Dimensions.height15),
                              Icon(Icons.arrow_drop_down_rounded)
                            ],
                          ),
                        ],
                      ),
                      Container(
                        height: Dimensions.height45,
                        width: Dimensions.height45,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Dimensions.radius15),
                          color: AppColors.mainColor
                        ),
                        child: Icon(Icons.search,color: Colors.white.withOpacity(0.8),size: Dimensions.iconsize24,),
                      ),
                  ],
                ),
              ),
            ),
            Expanded(child: SingleChildScrollView(child: FoodPageBody())),
          ],
        ),
      ),
    );
  }
}